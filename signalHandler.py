import sys, signal

def handler(signum = None, frame = None):
    print 'Signal' + signum + 'caught, system is about to quit'
    sys.exit(0)

def bindSignalAndHandler():
    for sig in [signal.SIGTERM, signal.SIGINT, signal.SIGHUP, signal.SIGQUIT]:
        signal.signal(sig, handler)
