from aenum import Enum
'''
Define enums for the Laser Atlas IPC protocol
'''
class LAProtocolMessageIndex(Enum):
    '''
    Define the content of the message
    bytes according to the index    
    '''
    SOURCE_DEVICE = 0
    DESTINATION_DEVICE = 1
    MESSAGE_TYPE = 2
    MESSAGE_SUB_TYPE = 3
    MESSAGE_CONTENT = 4
    MESSAGE_MSG_IDX_SIZE = 5
    
class LAProtocolDeviceID(Enum):
    ''' Device ID '''
    DEVICE_ID_NULL = 0
    DEVICE_ID_SCREEN = 1
    DEVICE_ID_XSENSE = 2
    DEVICE_ID_UTM_VERTICAL = 3
    DEVICE_ID_UTM_HORIZONTAL = 4
    DEVICE_ID_SIZE = 4

class LAProtocolMessageType(Enum):
    ''' Type of the message '''
    MESSAGE_NULL = 0
    MESSAGE_CMD = 1
    MESSAGE_STATUS = 2
    MESSAGE_LOG = 3
    MESSAGE_TYPE_SIZE = 4

class LAProtocolLogMessageID(Enum):
    ''' Type id of the log message '''
    NUL = 0
    INFO = 1
    DEBUG = 2
    ERROR = 3

class LAProtocolCommandID(Enum):
    ''' ID of the command '''
    CMD_NULL = 0
    CMD_START = 1
    CMD_STOP = 2
    CMD_QUERY = 3

class LAProtocolDeviceStatus(Enum):
    ''' ID of the device status '''
    DEVICE_STATUS_NULL = 0
    DEVICE_STATUS_INITIALIZING = 1
    DEVICE_STATUS_READY_TO_RUN = 2
    DEVICE_STATUS_RUNNING = 3
    DEVICE_STATUS_SAVING = 4
    DEVICE_STATUS_STOPPED = 5
    DEVICE_STATUS_SIZE = 6

