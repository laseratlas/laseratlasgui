''' Laser Atlas Communication bus Driver
    is for providing the interface for
    handling the IPC messages between
    different device programs.

    Protocol:
    Message in Byte: 0  - [SOURCE DEVICE ID]
                     1  - [DESTINATION DEVICE ID]
                     2  - [MESSAGE TYPE]
                     3  - [MESSAGE SUB TYPE]
                     .. - [CONTENTS DEPEND ON TYPES]
'''
import re
import os
import socket
import sys
import errno
from time import sleep
import threading
from laDataTypes import *
from laComBusMessageHandler import *

# list to store the identity of com bus client
_bus_clients = []
_bus_client_tasks = []
_bus_task_running_flag = False
_bus_lk = threading.Lock()
_bus_task = None
_bus = None
_BUS_RECV_MAX_LEN = 512
_BUS_NAME = "/tmp/laComBus"
_DEBUG_ENABLE_ = True

def driverInit():
    global _bus
    # Return if socket already created
    if _bus != None:
        return
    
    # Make sure the socket does not already exist 
    try:
        os.unlink(_BUS_NAME)
    except OSError:
        if os.path.exists(_BUS_NAME):
            raise
    # Create the socket
    _bus = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    log('Socket create' + str(_bus))
    _bus.setblocking(0)
    _bus.bind(_BUS_NAME)   
    # Listen for incomming connections
    log('Start listen')
    _bus.listen(1)

def driverClose():
    global _bus
    global _bus_task
    global _bus_clients
    global _bus_client_tasks
    
    for i in _bus_client_tasks:
        del i
    
    for i in _bus_clients:
        del i
    
    if _bus_task != None:
        _bus_task = None
    
    if _bus != None:
        _bus.close()
        _bus = None

def run():
    global _bus_client_tasks
    clientTask = None
    while getTaskRunningStatus():
        try:
            connection, client_addr = _bus.accept()
        except socket.error, e:
            err = e.args[0]
            if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                sleep(1)
                continue
            else:
                # a "real" error occurred
                log("Socket Error" + str(e))
                sys.exit(1)
        else:    
            log('New Connection' + str(connection) + ', Address ' + str(client_addr))
            #print 'New Connection' + str(connection) + ', Address ' + str(client_addr)
            clientTask = threading.Thread(target=client_rx_task, args=(connection,))
            _bus_client_tasks.append(clientTask)
            clientTask.start()
    #print 'Driver task quit'
    log('Driver task quit')
    return

def client_rx_task(connection):
    connection.setblocking(0)
    addNewClient(connection)
    while getTaskRunningStatus():
        try:
            data = connection.recv(_BUS_RECV_MAX_LEN)
            if data:
                strlist = filter(bool, re.split('\n', data))
                #print strlist
                for line in strlist:
                    addNewComBusRxMessage(line)
        except socket.error, e:
            err = e.args[0]
            if err == errno.EAGAIN or err == errno.EWOULDBLOCK or err == errno.EPIPE:
                sleep(1)
                continue
            else:
                # a "real" error occurred
                log("Socket Error" + str(e))
                #sys.exit(1)
            pass
    connection.close()
    removeClient(connection)
    log('Client task quit: ' + str(connection))
    #print 'Client task quit: ' + str(connection)
    return

def startDriverTask():
    global _bus_task
    # return if already running
    if getTaskRunningStatus():
        return
    
    setTaskRunningStatus(True)
    _bus_task = threading.Thread(target=run)
    _bus_task.start()

def stopDriverTask():
    # Return if already stopped
    if not getTaskRunningStatus():
        return
    setTaskRunningStatus(False)

def getTaskRunningStatus():
    flag = False
    _bus_lk.acquire()
    flag = _bus_task_running_flag
    _bus_lk.release()
    return flag

def setTaskRunningStatus(flag):
    global _bus_task_running_flag
    _bus_lk.acquire()
    _bus_task_running_flag = flag
    _bus_lk.release()

def addNewClient(client):
    global _bus_clients
    _bus_lk.acquire()
    _bus_clients.append(client)
    _bus_lk.release()

def removeClient(cilent):
    global _bus_clients
    _bus_lk.acquire()
    _bus_clients.remove(client)
    _bus_lk.release()

def getClientsNum():
    num = 0
    _bus_lk.acquire()
    num = len(_bus_clients)
    _bus_lk.release()
    return num

def getClientByIndex(id):
    client = None
    _bus_lk.acquire()
    client = _bus_clients[id]
    _bus_lk.release()
    return client

def joinDriverTaskThread():
    _bus_task.join()

def driverSendMessage(msg):
    #Directly send message to devices
    #buffer is not needed

    num = getClientsNum()
    for i in range(0, num):
        client = getClientByIndex(i)
        # Try to send up to three times if needed
        for i in range(0, 3):
            try:
                client.sendall(msg)
            except socket.error, e:
                err = e.args[0]
                if err == errno.EAGAIN or err == errno.EWOULDBLOCK or err == errno.EPIPE:
                    continue
                else:
                    # a "real" error occurred
                    log("Socket Error" + str(e))
                    #sys.exit(1)
            else:
                break

def driverComBusMessageCreate(source, destination, typeId, subTypeId, content=''):
    msg =''
    msg += str(source)
    msg += ':'
    msg += str(destination)
    msg += ':'
    msg += str(typeId)
    msg += ':'
    msg += str(subTypeId)
    msg += ':'
    msg += str(content)
    msg += '\n'
    return msg

def extractInfoFromMessage(infoList, msg):
    comma_index = [pos for pos, char in enumerate(msg) if char == ':']
    if len(comma_index) < 4:
        return False
    # 0
    source = int(msg[:comma_index[0]])
    if source > LAProtocolDeviceID.DEVICE_ID_SIZE.value:
        return False
    infoList.append(source)
    #print "Source " + str(source)
    # 1
    destination = int(msg[comma_index[0]+1:comma_index[1]])
    if destination != LAProtocolDeviceID.DEVICE_ID_SCREEN.value:
        return False
    infoList.append(destination)
    #print "destination " + str(destination)
    # 2
    typeId = int(msg[comma_index[1]+1:comma_index[2]])
    if typeId <= LAProtocolMessageType.MESSAGE_NULL.value or typeId >= LAProtocolMessageType.MESSAGE_TYPE_SIZE.value:
        return False
    infoList.append(typeId)
    #print "type " + str(typeId)
    # 3
    subType = int(msg[comma_index[2]+1:comma_index[3]])
    if subType == 0:
        return False
    infoList.append(subType)
    #print "subType " + str(subType)
    # 4
    content = msg[comma_index[3]+1:]
    infoList.append(content)
    #print "Content " + content
    return True

def log(msg):
    if _DEBUG_ENABLE_:
        source = LAProtocolDeviceID.DEVICE_ID_SCREEN.value
        destination = LAProtocolDeviceID.DEVICE_ID_SCREEN.value
        typeID = LAProtocolMessageType.MESSAGE_LOG.value
        logID = LAProtocolLogMessageID.DEBUG.value
        message = driverComBusMessageCreate(source, destination, typeID, logID, msg)
        addNewComBusRxMessage(message)

    
