import sys
import threading
import time
from PyQt4 import QtCore, QtGui, uic
import subprocess
from laWindow import LAWindow
from laTaskController import LATaskController
from laComBusMessageHandler import *
from laComBusDriver import *
from laDataTypes import *

def test_thread():
    time.sleep(2)
    
    # fake status msg for XSens
    source = LAProtocolDeviceID.DEVICE_ID_XSENSE.value
    destination = LAProtocolDeviceID.DEVICE_ID_SCREEN.value
    typeID = LAProtocolMessageType.MESSAGE_STATUS.value
    statusID = LAProtocolDeviceStatus.DEVICE_STATUS_READY_TO_RUN.value
    msg = driverComBusMessageCreate(source, destination, typeID, statusID)
    addNewComBusRxMessage(msg)
    time.sleep(2)
    
    # fake status msg for vertical laser sensor
    source = LAProtocolDeviceID.DEVICE_ID_UTM_VERTICAL.value
    destination = LAProtocolDeviceID.DEVICE_ID_SCREEN.value
    typeID = LAProtocolMessageType.MESSAGE_STATUS.value
    statusID = LAProtocolDeviceStatus.DEVICE_STATUS_READY_TO_RUN.value
    msg = driverComBusMessageCreate(source, destination, typeID, statusID)
    addNewComBusRxMessage(msg)

def initSensorProcesses():
    subprocess.Popen(["/home/pi/laser_atlas/XSenseReader"])
    subprocess.Popen(["/home/pi/laser_atlas/UTMSensorReader"])
    pass

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = LAWindow()

    taskController = LATaskController()
    # connect the slots and signals
    taskController.log_message.connect(window.addLogMessage)
    taskController.buttons_status_change.connect(window.setButtonStatus)
    window.button_action.connect(taskController.gui_button_action_triggled)

    # Send task controller reference to message handler
    initComMessageHandler(taskController)
    startMessageHandlerTask()

    # Init and Start the ComBus driver
    driverInit()
    startDriverTask()
    
    window.addLogMessage('********************************')
    window.addLogMessage('* LASER ATLAS COPY RIGHT 2016  *')
    window.addLogMessage('********************************')
    window.addLogMessage('\nInitializing...')
    window.showFullScreen()
    # Run Test thread
    #testTh = threading.Thread(target=test_thread)
    #testTh.start()

    initSensorProcesses()
    exitReturn = app.exec_()
    joinDriverTaskThread()
    sys.exit(exitReturn)
