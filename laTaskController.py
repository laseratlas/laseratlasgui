from PyQt4.QtCore import QObject, pyqtSignal
from laWindow import LAButtonID
from laComBusMessageParser import LAComBusMessageParser
from laDataTypes import *
from subprocess import PIPE, Popen, check_output
from laComBusDriver import *
import subprocess

class LATaskController(QObject, LAComBusMessageParser):
    def __init__(self):
        QObject.__init__(self)
        self.stopFlag = False
        self.devicesStatusFlag = [LAProtocolDeviceStatus.DEVICE_STATUS_STOPPED.value]*4
    # Add a com message to the devices according to the button ID
    # Define a signal to add log message to the Qt gui
    log_message = pyqtSignal('QString', int, int, name="logMessage")
    # Define a signal to change the buttons 
    buttons_status_change = pyqtSignal(int, bool, name="buttonStatusChange")

    def gui_button_action_triggled(self, buttonID):
        cmd_msg = ''
        cmd = LAProtocolCommandID.CMD_NULL.value

        print 'button ID: ' + str(buttonID)
        if buttonID == LAButtonID.START_BUTTON.value:
            cmd = LAProtocolCommandID.CMD_START.value
        elif buttonID == LAButtonID.STOP_BUTTON.value:
            cmd = LAProtocolCommandID.CMD_STOP.value
            self.stopFlag = True
        elif buttonID == LAButtonID.REBOOT_BUTTON.value:
            self.log_message.emit('System is about to reboot', LAProtocolLogMessageID.INFO.value, LAProtocolDeviceID.DEVICE_ID_SCREEN.value)
            self.restartSys()
            return

        # Do extra actions first
        self.extraButtonCMDAction(cmd)
        
        self.log_message.emit('button pressed', LAProtocolLogMessageID.DEBUG.value, LAProtocolDeviceID.DEVICE_ID_SCREEN.value)
        source = LAProtocolDeviceID.DEVICE_ID_SCREEN.value
        destination = LAProtocolDeviceID.DEVICE_ID_NULL.value
        typeId = LAProtocolMessageType.MESSAGE_CMD.value
        msg = driverComBusMessageCreate(source, destination, typeId, cmd)
        
        self.log_message.emit(msg, LAProtocolLogMessageID.DEBUG.value, LAProtocolDeviceID.DEVICE_ID_SCREEN.value)
        driverSendMessage(msg)

    def parse(self, msg):
        infoList = []
        
        if not extractInfoFromMessage(infoList, msg):
            return

        source = infoList[LAProtocolMessageIndex.SOURCE_DEVICE.value]
        typeId = infoList[LAProtocolMessageIndex.MESSAGE_TYPE.value]
        subType = infoList[LAProtocolMessageIndex.MESSAGE_SUB_TYPE.value]
        content = infoList[LAProtocolMessageIndex.MESSAGE_CONTENT.value]
        
        if typeId == LAProtocolMessageType.MESSAGE_STATUS.value:
            self.deviceStatusReceived(source, subType)
        elif typeId == LAProtocolMessageType.MESSAGE_LOG.value: 
            self.deviceLogReceived(source, subType, content)
        else:
            pass

    def restartSys(self):
        #execute reboot cmd
        out = check_output(["sudo", "reboot"])
        if len(out) != 0:
            self.log_message.emit(out, LAProtocolLogMessageID.INFO.value, LAProtocolDeviceID.DEVICE_ID_SCREEN.value)

    def deviceStatusReceived(self, deviceID, status):
        if status >= LAProtocolDeviceStatus.DEVICE_STATUS_SIZE.value:
            return
        # check if new device status
        if self.devicesStatusFlag[deviceID] != status:
           self.devicesStatusFlag[deviceID] = status
           self.doDeviceStatusChangedAction()
        pass

    def deviceLogReceived(self, deviceID, logType, msg):
        self.log_message.emit(msg, logType, deviceID)

    def doDeviceStatusChangedAction(self):
        # Check if all the device are in the same status first
        xsensStatus = self.devicesStatusFlag[LAProtocolDeviceID.DEVICE_ID_XSENSE.value]
        utmVStatus = self.devicesStatusFlag[LAProtocolDeviceID.DEVICE_ID_UTM_VERTICAL.value]

        # If not return, because action can be done only
        # when all the devices are changed to same sattus
        if(xsensStatus != utmVStatus):
            return;

        if xsensStatus == LAProtocolDeviceStatus.DEVICE_STATUS_READY_TO_RUN.value:
            # Enable START button when all the devices are ready to run
            self.buttons_status_change.emit(LAButtonID.START_BUTTON.value, True)
            self.log_message.emit('START button enabled', LAProtocolLogMessageID.DEBUG.value, LAProtocolDeviceID.DEVICE_ID_SCREEN.value)
        elif xsensStatus == LAProtocolDeviceStatus.DEVICE_STATUS_RUNNING.value:
            # Enable STOP button when all the devices are running
            self.buttons_status_change.emit(LAButtonID.STOP_BUTTON.value, True)
            self.log_message.emit('STOP button enabled', LAProtocolLogMessageID.DEBUG.value, LAProtocolDeviceID.DEVICE_ID_SCREEN.value)
        elif xsensStatus == LAProtocolDeviceStatus.DEVICE_STATUS_STOPPED:
            # Enable REBOOT button when all the devices are stopped
            self.log_message.emit('REBOOT button enabled', LAProtocolLogMessageID.DEBUG.value, LAProtocolDeviceID.DEVICE_ID_SCREEN.value)
            self.buttons_status_change.emit(LAButtonID.STOP_BUTTON.value, True)

    def extraButtonCMDAction(self, cmd):
        # START or STOP the wlanscanner task depends on the CMD
        if cmd == LAProtocolCommandID.CMD_START.value:
            self.log_message.emit('Wlan scanner running', LAProtocolLogMessageID.INFO.value, LAProtocolDeviceID.DEVICE_ID_SCREEN.value)
            subprocess.Popen(["/home/pi/laser_atlas/watch_per_second.sh"])
        elif cmd == LAProtocolCommandID.CMD_STOP.value:
            self.log_message.emit('Wlan scanner stop', LAProtocolLogMessageID.INFO.value, LAProtocolDeviceID.DEVICE_ID_SCREEN.value)
            subprocess.Popen(["killall", "watch"])

