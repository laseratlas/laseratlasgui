import sys
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtCore import QObject, pyqtSignal
from aenum import Enum
from laDataTypes import LAProtocolLogMessageID, LAProtocolDeviceID

class LAButtonID(Enum):
    NULL_BUTTON = 0
    START_BUTTON = 1
    STOP_BUTTON = 2
    BACKUP_BUTTON = 3
    REBOOT_BUTTON = 4
    
class LAWindow(QtGui.QMainWindow):
    def __init__(self):
        super(LAWindow, self).__init__()
        uic.loadUi('laserAtlasWindow.ui', self)
        self.connectUiSignalsAndSlots()
        self.setButtonStatus(LAButtonID.START_BUTTON.value, False)
        self.setButtonStatus(LAButtonID.STOP_BUTTON.value, False)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)

    button_action = pyqtSignal(int, name="buttonAction")
    
    def connectUiSignalsAndSlots(self):
        ''' Connect the UI button press events to the python slots '''
        self.startButton.released.connect(self.startAction)
        self.stopButton.released.connect(self.stopAction)
        self.rebootButton.released.connect(self.rebootAction)

    def disableUiButtons(self):
        ''' Disable UI buttons '''
        self.setButtonStatus(LAButtonID.START_BUTTON.value, False)
        self.setButtonStatus(LAButtonID.STOP_BUTTON.value, False)
        self.setButtonStatus(LAButtonID.BACKUP_BUTTON.value, False)
        self.setButtonStatus(LAButtonID.REBOOT_BUTTON.value, False)

    def enableUiButtons(self):
        ''' Enable all UI buttons '''
        self.setButtonStatus(LAButtonID.START_BUTTON.value, True)
        self.setButtonStatus(LAButtonID.STOP_BUTTON.value, True)
        self.setButtonStatus(LAButtonID.BACKUP_BUTTON.value, True)
        self.setButtonStatus(LAButtonID.REBOOT_BUTTON.value, True)

    def setButtonStatus(self, buttonID, status):
        '''Change the status of button'''
        if buttonID == LAButtonID.START_BUTTON.value:
            self.startButton.setEnabled(status)
        elif buttonID == LAButtonID.STOP_BUTTON.value:
            self.stopButton.setEnabled(status)
        elif buttonID == LAButtonID.BACKUP_BUTTON.value:
            self.backupButton.setEnabled(status)
        elif buttonID == LAButtonID.REBOOT_BUTTON.value:
            self.rebootButton.setEnabled(status)
        
    def startAction(self):
        ''' Start button event handler '''
        self.addLogMessage('Start action.', LAProtocolLogMessageID.DEBUG.value)
        self.setButtonStatus(LAButtonID.START_BUTTON.value, False)
        #send START command to start scanning
        self.button_action.emit(LAButtonID.START_BUTTON.value)
        
    def stopAction(self):
        ''' Stop button event handler '''
        self.addLogMessage('Stop action', LAProtocolLogMessageID.DEBUG.value)
        self.setButtonStatus(LAButtonID.STOP_BUTTON.value, False)
        #send STOP command to start scanning
        self.button_action.emit(LAButtonID.STOP_BUTTON.value)
        
    def rebootAction(self):
        ''' Reboot button event handler '''
        self.addLogMessage('Reboot action', LAProtocolLogMessageID.DEBUG.value)
        self.setButtonStatus(LAButtonID.REBOOT_BUTTON.value, False)
        # REBOOT the RPI 
        self.button_action.emit(LAButtonID.REBOOT_BUTTON.value)

    def backupAction(self):
        ''' Backup button event handler '''
        self.addLogMessage('Backup action', LAProtocolLogMessageID.DEBUG.value)
        self.setButtonStatus(LAButtonID.BACKUP_BUTTON.value, False)
        # Send backup sensor data signal
        self.button_action.emit(LAButtonID.BACKUP_BUTTON.value)

    def addLogMessage(self, msg, Id=LAProtocolLogMessageID.NUL.value, source=LAProtocolDeviceID.DEVICE_ID_SCREEN.value):
        ''' Add specified new log message and display in the textBrowser '''
	msg_header = ''
        if Id == LAProtocolLogMessageID.NUL.value:
            pass
        elif Id == LAProtocolLogMessageID.INFO.value:
            msg_header += '[INFO] '
        elif Id == LAProtocolLogMessageID.DEBUG.value:
            msg_header += '[DEBUG] '
        elif Id == LAProtocolLogMessageID.ERROR.value:
            msg_header += '[ERROR] '
        else:
            msg_header += '[INFO] '
        
        if source != LAProtocolDeviceID.DEVICE_ID_SCREEN.value:
            msg_header += 'Source: '
            msg_header += str(source)
            msg_header += ' '
            
        msg_header += str(msg)
        self.textBrowser.append(msg_header)

