import laDataTypes
import threading
from laComBusMessageParser import LAComBusMessageParser

# static member list for rx data
_rx_data_fifo = []
# static member list for tx data
_tx_data_fifo = []
# static member for synchronize shared data access
_task_lk = threading.Lock()
#static member to notice the _msg_fifo
_wait_condition = threading.Condition(_task_lk)
#static flag for threading running status
_msg_handler_running_status = False
#satic thread for running the driver task
_thread = None
#ComBusMessageParser
_parser = None
 
def initComMessageHandler(parserObj):
    ''' need to be called before start'''
    global _parser
    _parser = parserObj
    
def getMessageHandlerTaskRunningStatus():
    flag = False
    _wait_condition.acquire()
    flag = _msg_handler_running_status
    _wait_condition.release()
    return flag

def setMessageHandlerTaskRunningStatus(flag):
    global _msg_handler_running_status
    _wait_condition.acquire()
    _msg_handler_running_status = flag
    if not flag:
        _wait_condition.notify()    
    _wait_condition.release()

def addNewComBusRxMessage(data):
    _wait_condition.acquire()
    _rx_data_fifo.append(data)
    _wait_condition.notify()    
    _wait_condition.release()

def popOneRxMessageFromFifo():
    msg = ''
    _wait_condition.acquire()
    msg = _rx_data_fifo.pop(0)  
    _wait_condition.release()
    return msg;

def isRxMessageFifoEmpty():
    size = 0
    _wait_condition.acquire()
    size = len(_rx_data_fifo)    
    _wait_condition.release()
    return size;
    
def startMessageHandlerTask():
    global _thread
    if getMessageHandlerTaskRunningStatus():
        return
    
    setMessageHandlerTaskRunningStatus(True)

    _thread = threading.Thread(target=taskRun)
    _thread.start()

def stopMessageHandlerTask():
    setMessageHandlerTaskRunningStatus(False)

# need to be called after lock acquired 
def isWaitConditionMet():
    #print 'Debug: msg fifo ' + str(_rx_data_fifo) + ' flag ' + str(_msg_handler_running_status)
    return (_rx_data_fifo or not _msg_handler_running_status)

def taskRun():
    msg = ''
    while True:
        _wait_condition.acquire()
        while not isWaitConditionMet():
            _wait_condition.wait()
        _wait_condition.release()
        
        if not getMessageHandlerTaskRunningStatus():
            return

        if _parser is None:
            continue;
        while isRxMessageFifoEmpty() != 0:
            msg = popOneRxMessageFromFifo() 
            _parser.parse(msg)
    pass #end
